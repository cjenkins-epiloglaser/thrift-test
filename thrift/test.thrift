namespace cpp epilog.test

service TestService
{
  i32 getSerialNumber()
  string getModelName()
  string getMachineName()
}
