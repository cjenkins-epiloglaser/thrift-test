Requires CMake, thrift and their dependencies to build.

On Windows run makewinproject.bat and open the Visual Studio Project created in the build_win folder.

On Linux:
```
mkdir build_linux
cd build_linux
cmake -G "Unix Makefiles" ..
make
```