#include "TestService.h"
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/protocol/TMultiplexedProtocol.h>
#include <thrift/protocol/TProtocol.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TTransport.h>

#include <memory>
#include <iostream>

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;

using namespace ::epilog::test;

int main(int argc, char** argv)
{
	std::shared_ptr<TTransport> transport(new TSocket("localhost", 9090));
	transport->open();

	std::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));

	std::shared_ptr<TProtocol> testMultiplexedProtocol(new TMultiplexedProtocol(protocol, "Test"));
	std::shared_ptr<TestServiceClient> client(new TestServiceClient(testMultiplexedProtocol));

	std::cout << "Serial: " << client->getSerialNumber() << std::endl;

	std::string model;
	client->getModelName(model);
	std::cout << "Model: " << model << std::endl;

	std::string name;
	client->getMachineName(name);
	std::cout << "Name: " << name << std::endl;

	transport->close();
}
