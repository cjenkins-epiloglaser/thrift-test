cmake_minimum_required(VERSION 2.8)

project(TestThrift C CXX)

set(THRIFT_SRC "${CMAKE_SOURCE_DIR}/gen-thrift/test_constants.cpp"
	       "${CMAKE_SOURCE_DIR}/gen-thrift/test_constants.h"
	       "${CMAKE_SOURCE_DIR}/gen-thrift/test_types.cpp"
	       "${CMAKE_SOURCE_DIR}/gen-thrift/test_types.h"
	       "${CMAKE_SOURCE_DIR}/gen-thrift/TestService.cpp"
	       "${CMAKE_SOURCE_DIR}/gen-thrift/TestService.h"
           )

add_custom_target(build-time-make-directory ALL
    COMMAND ${CMAKE_COMMAND} -E make_directory "${CMAKE_SOURCE_DIR}/gen-thrift"
)

add_custom_command(
    COMMAND thrift -out ${CMAKE_SOURCE_DIR}/gen-thrift/ --gen cpp ${CMAKE_SOURCE_DIR}/thrift/test.thrift
    DEPENDS ${CMAKE_SOURCE_DIR}/thrift/test.thrift
    OUTPUT ${THRIFT_SRC}
)

if(MSVC)
    include_directories(
        "${CMAKE_SOURCE_DIR}/gen-thrift"
        "C:/tools/vcpkg/installed/x86-windows/include"
        )
    link_directories(
        "C:/tools/vcpkg/installed/x86-windows/lib"
        "C:/tools/vcpkg/installed/x86-windows/debug/lib"
        )

    link_libraries(
        debug thriftmdd optimized thriftmd
        debug thriftnbmdd optimized thriftnbmd
        debug thriftzmdd optimized thriftzmd
        libeay32
        ssleay32
        )
else()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

    include_directories(
        "${CMAKE_SOURCE_DIR}/gen-thrift"
        "/usr/include"
        )
    link_directories(
	/usr/lib
	)

    link_libraries(
	event
        thrift
	thriftnb
	thriftz
	z
        )
endif()

add_executable(test-thrift-client
    "${CMAKE_SOURCE_DIR}/src/thrift-client.cpp"
    ${THRIFT_SRC}
    )

add_executable(test-thrift-server
    "${CMAKE_SOURCE_DIR}/src/thrift-server.cpp"
    ${THRIFT_SRC}
    )
